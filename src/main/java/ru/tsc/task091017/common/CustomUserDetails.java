package ru.tsc.task091017.common;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;

public class CustomUserDetails extends User {
    private Long id;

    public CustomUserDetails(String name, String pass, Collection<? extends GrantedAuthority> auth, Long id) {
        super(name, pass, auth);
        this.id = id;
    }

    public Long getId() {
        return id;
    }

}