package ru.tsc.task091017.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ru.tsc.task091017.dao.domain.Domains;
import ru.tsc.task091017.dao.service.DomainsService;

@Controller
@RequestMapping(value = "rest/Domains")
public class DomainsController {
    public static final String GET = "/get/{id}";
    public static final String ADD = "/add";
    public static final String UPDATE = "/update";
    public static final String DELETE = "/delete/{id}";

    private final DomainsService domainsService;

    @Autowired
    public DomainsController(DomainsService domainsService) {
        this.domainsService = domainsService;
    }

    @RequestMapping(value = ADD, method = RequestMethod.POST, consumes = "application/json;charset=utf-8")
    public ResponseEntity createDomains(@RequestBody Domains domains) {
        domainsService.createDomains(domains);
        return new ResponseEntity(HttpStatus.CREATED);
    }

    @RequestMapping(value = GET, method = RequestMethod.GET, produces = "application/json;charset=utf-8")
    @ResponseBody
    public ResponseEntity<Domains> retrieveDomains(@PathVariable("id") Long id) {
        return new ResponseEntity<>(domainsService.retrieveDomains(id), HttpStatus.OK);
    }

    @RequestMapping(value = UPDATE, method = RequestMethod.PUT, consumes = "application/json;charset=utf-8")
    public ResponseEntity updateDomains(@RequestBody Domains domains) {
        domainsService.updateDomains(domains);
        return new ResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = DELETE, method = RequestMethod.DELETE)
    public ResponseEntity deleteDomains(@PathVariable("id") Long id) {
        domainsService.deleteDomains(id);
        return new ResponseEntity(HttpStatus.OK);
    }

}
