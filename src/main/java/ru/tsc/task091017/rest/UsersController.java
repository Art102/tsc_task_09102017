package ru.tsc.task091017.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ru.tsc.task091017.dao.domain.Users;
import ru.tsc.task091017.dao.service.UsersService;
import ru.tsc.task091017.rest.util.UserAccessUtility;

@Controller
@RequestMapping(value = "rest/Users")
public class UsersController {
    public static final String GET = "/get/{id}";
    public static final String ADD = "/add";
    public static final String UPDATE = "/update";
    public static final String DELETE = "/delete/{id}";

    private final UsersService usersService;

    @Autowired
    public UsersController(UsersService usersService) {
        this.usersService = usersService;
    }

    @RequestMapping(value = ADD, method = RequestMethod.POST, consumes = "application/json;charset=utf-8")
    public ResponseEntity createUsers(@RequestBody Users users) {
        usersService.createUsers(users);
        return new ResponseEntity(HttpStatus.CREATED);
    }

    @RequestMapping(value = GET, method = RequestMethod.GET, produces = "application/json;charset=utf-8")
    @ResponseBody
    public ResponseEntity<Users> retrieveUsers(@PathVariable("id") Long id) {
        return new ResponseEntity<>(usersService.retrieveUsers(id), HttpStatus.OK);
    }

    @RequestMapping(value = UPDATE, method = RequestMethod.PUT, consumes = "application/json;charset=utf-8")
    public ResponseEntity updateUsers(@RequestBody Users users) {
        HttpStatus status = UserAccessUtility.checkAccess(users.getId());
        if (HttpStatus.OK.equals(status)) {
            usersService.updateUsers(users);
            return new ResponseEntity(status);
        } else {
            return new ResponseEntity(status);
        }
    }

    @RequestMapping(value = DELETE, method = RequestMethod.DELETE)
    public ResponseEntity deleteUsers(@PathVariable("id") Long id) {
        HttpStatus status = UserAccessUtility.checkAccess(id);
        if (HttpStatus.OK.equals(status)) {
            usersService.deleteUsers(id);
            return new ResponseEntity(status);
        } else {
            return new ResponseEntity(status);
        }
    }
}
