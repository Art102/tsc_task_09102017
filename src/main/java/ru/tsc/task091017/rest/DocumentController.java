package ru.tsc.task091017.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ru.tsc.task091017.dao.domain.Document;
import ru.tsc.task091017.dao.domain.TypeStructure;
import ru.tsc.task091017.dao.service.DocumentService;
import ru.tsc.task091017.rest.util.UserAccessUtility;

import java.util.List;

@Controller
@RequestMapping(value = "rest/Document")
public class DocumentController {
    public static final String GET = "/get/{id}";
    public static final String ADD = "/add";
    public static final String UPDATE = "/update";
    public static final String DELETE = "/delete/{id}";
    public static final String GET_TYPES_BY_OWNER = "/GetTypesByOwner/{ownerId}";

    private final DocumentService documentService;

    @Autowired
    public DocumentController(DocumentService documentService) {
        this.documentService = documentService;
    }

    @RequestMapping(value = GET_TYPES_BY_OWNER, method = RequestMethod.GET,
            produces = "application/json;charset=utf-8")
    @ResponseBody
    public ResponseEntity<List<TypeStructure>> getDocumentTypesByOwner(@PathVariable("ownerId") Long ownerId) {
        return new ResponseEntity<>(documentService.getDocumentTypesByOwner(ownerId), HttpStatus.OK);
    }

    @RequestMapping(value = ADD, method = RequestMethod.POST, consumes = "application/json;charset=utf-8")
    public ResponseEntity createDocument(@RequestBody Document document) {
        documentService.createDocument(document);
        return new ResponseEntity(HttpStatus.CREATED);
    }

    @RequestMapping(value = GET, method = RequestMethod.GET, produces = "application/json;charset=utf-8")
    @ResponseBody
    public ResponseEntity<Document> retrieveDocument(@PathVariable("id") Long id) {
        return new ResponseEntity<>(documentService.retrieveDocument(id), HttpStatus.OK);
    }

    @RequestMapping(value = UPDATE, method = RequestMethod.PUT, consumes = "application/json;charset=utf-8")
    public ResponseEntity updateDocument(@RequestBody Document document) {
        HttpStatus status = UserAccessUtility.checkAccess(document.getOwner().getId());
        if (HttpStatus.OK.equals(status)) {
            documentService.updateDocument(document);
            return new ResponseEntity(status);
        } else {
            return new ResponseEntity(status);
        }
    }

    @RequestMapping(value = DELETE, method = RequestMethod.DELETE)
    public ResponseEntity deleteDocument(@PathVariable("id") Long id) {
        HttpStatus status = UserAccessUtility.checkAccess(id);
        if (HttpStatus.OK.equals(status)) {
            documentService.deleteDocument(id);
            return new ResponseEntity(status);
        } else {
            return new ResponseEntity(status);
        }
    }

}
