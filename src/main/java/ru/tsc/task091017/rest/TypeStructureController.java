package ru.tsc.task091017.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ru.tsc.task091017.dao.domain.TypeStructure;
import ru.tsc.task091017.dao.service.TypeStructureService;
import ru.tsc.task091017.rest.util.UserAccessUtility;

@Controller
@RequestMapping(value = "rest/TypeStructure")
public class TypeStructureController {
    public static final String GET = "/get/{id}";
    public static final String ADD = "/add";
    public static final String UPDATE = "/update";
    public static final String DELETE = "/delete/{id}";

    private final TypeStructureService typeStructureService;

    @Autowired
    public TypeStructureController(TypeStructureService typeStructureService) {
        this.typeStructureService = typeStructureService;
    }

    @RequestMapping(value = ADD, method = RequestMethod.POST, consumes = "application/json;charset=utf-8")
    public ResponseEntity createTypeStructure(@RequestBody TypeStructure typeStructure) {
        typeStructureService.createTypeStructure(typeStructure);
        return new ResponseEntity(HttpStatus.CREATED);
    }

    @RequestMapping(value = GET, method = RequestMethod.GET, produces = "application/json;charset=utf-8")
    @ResponseBody
    public ResponseEntity<TypeStructure> retrieveTypeStructure(@PathVariable("id") Long id) {
        return new ResponseEntity<>(typeStructureService.retrieveTypeStructure(id), HttpStatus.OK);
    }

    @RequestMapping(value = UPDATE, method = RequestMethod.PUT, consumes = "application/json;charset=utf-8")
    public ResponseEntity updateTypeStructure(@RequestBody TypeStructure typeStructure) {
        HttpStatus status = UserAccessUtility.checkAccess(typeStructure.getOwner().getId());
        if (HttpStatus.OK.equals(status)) {
            typeStructureService.updateTypeStructure(typeStructure);
            return new ResponseEntity(status);
        } else {
            return new ResponseEntity(status);
        }
    }

    @RequestMapping(value = DELETE, method = RequestMethod.DELETE)
    public ResponseEntity deleteTypeStructure(@PathVariable("id") Long id) {
        HttpStatus status = UserAccessUtility.checkAccess(id);
        if (HttpStatus.OK.equals(status)) {
            typeStructureService.deleteTypeStructure(id);
            return new ResponseEntity(status);
        } else {
            return new ResponseEntity(status);
        }
    }
}
