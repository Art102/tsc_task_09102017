package ru.tsc.task091017.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ru.tsc.task091017.dao.domain.FolderDocTypeRestriction;
import ru.tsc.task091017.dao.service.FolderDocTypeRestrictionService;
import ru.tsc.task091017.rest.util.UserAccessUtility;

@Controller
@RequestMapping(value = "rest/FolderDocTypeRestriction")
public class FolderDocTypeRestrictionController {
    public static final String GET = "/get/{id}";
    public static final String ADD = "/add";
    public static final String UPDATE = "/update";
    public static final String DELETE = "/delete/{id}";

    private final FolderDocTypeRestrictionService folderDocTypeRestrictionService;

    @Autowired
    public FolderDocTypeRestrictionController(FolderDocTypeRestrictionService folderDocTypeRestrictionService) {
        this.folderDocTypeRestrictionService = folderDocTypeRestrictionService;
    }

    @RequestMapping(value = ADD, method = RequestMethod.POST, consumes = "application/json;charset=utf-8")
    public ResponseEntity createFolderDocTypeRestriction(@RequestBody FolderDocTypeRestriction folderDocTypeRestriction) {
        folderDocTypeRestrictionService.createFolderDocTypeRestriction(folderDocTypeRestriction);
        return new ResponseEntity(HttpStatus.CREATED);
    }

    @RequestMapping(value = GET, method = RequestMethod.GET, produces = "application/json;charset=utf-8")
    @ResponseBody
    public ResponseEntity<FolderDocTypeRestriction> retrieveFolderDocTypeRestriction(@PathVariable("id") Long id) {
        return new ResponseEntity<>(folderDocTypeRestrictionService.retrieveFolderDocTypeRestriction(id), HttpStatus.OK);
    }

    @RequestMapping(value = UPDATE, method = RequestMethod.PUT, consumes = "application/json;charset=utf-8")
    public ResponseEntity updateFolderDocTypeRestriction(@RequestBody FolderDocTypeRestriction folderDocTypeRestriction) {
        HttpStatus status = UserAccessUtility.checkAccess(folderDocTypeRestriction.getOwner().getId());
        if (HttpStatus.OK.equals(status)) {
            folderDocTypeRestrictionService.updateFolderDocTypeRestriction(folderDocTypeRestriction);
            return new ResponseEntity(status);
        } else {
            return new ResponseEntity(status);
        }
    }

    @RequestMapping(value = DELETE, method = RequestMethod.DELETE)
    public ResponseEntity deleteFolderDocTypeRestriction(@PathVariable("id") Long id) {
        HttpStatus status = UserAccessUtility.checkAccess(id);
        if (HttpStatus.OK.equals(status)) {
            folderDocTypeRestrictionService.deleteFolderDocTypeRestriction(id);
            return new ResponseEntity(status);
        } else {
            return new ResponseEntity(status);
        }
    }
}
