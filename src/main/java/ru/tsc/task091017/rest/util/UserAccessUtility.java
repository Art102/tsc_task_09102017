package ru.tsc.task091017.rest.util;

import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import ru.tsc.task091017.common.CustomUserDetails;

import java.util.ArrayList;
import java.util.List;

public class UserAccessUtility {

    public static HttpStatus checkAccess(Long id) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication.getPrincipal().toString().equals("anonymousUser")) {
            return HttpStatus.UNAUTHORIZED;
        }

        CustomUserDetails user = (CustomUserDetails) authentication.getPrincipal();

        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.addAll(user.getAuthorities());
        List<String> roles = new ArrayList<>();
        for (GrantedAuthority ga : authorities) {
            roles.add(ga.getAuthority());
        }

        if (user.getId().equals(id)) {
            return HttpStatus.OK;
        } else if (!(roles.contains("ROLE_1") || roles.contains("ROLE_2"))) {
            return HttpStatus.OK;
        } else {
            return HttpStatus.NOT_ACCEPTABLE;
        }
    }

}
