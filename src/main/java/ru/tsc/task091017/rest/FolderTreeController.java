package ru.tsc.task091017.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ru.tsc.task091017.dao.domain.FolderTree;
import ru.tsc.task091017.dao.service.FolderTreeService;
import ru.tsc.task091017.dao.domain.FolderTreeWrapper;
import ru.tsc.task091017.rest.util.UserAccessUtility;

import java.util.List;

@Controller
@RequestMapping(value = "rest/FolderTree")
public class FolderTreeController {
    public static final String GET = "/get/{id}";
    public static final String ADD = "/add";
    public static final String UPDATE = "/update";
    public static final String DELETE = "/delete/{id}";
    public static final String GET_BY_ID = "/GetById/{id}";
    public static final String GET_BY_OWNER = "/GetByOwner/{ownerId}";

    private final FolderTreeService folderTreeService;

    @Autowired
    public FolderTreeController(FolderTreeService folderTreeService) {
        this.folderTreeService = folderTreeService;
    }

    @RequestMapping(value = GET_BY_ID, method = RequestMethod.GET, produces = "application/json;charset=utf-8")
    @ResponseBody
    public ResponseEntity<FolderTreeWrapper> getFolderTreeById(@PathVariable("id") Long id) {
        return new ResponseEntity<>(folderTreeService.getFolderTreeById(id), HttpStatus.OK);
    }

    @RequestMapping(value = GET_BY_OWNER, method = RequestMethod.GET, produces = "application/json;charset=utf-8")
    @ResponseBody
    public ResponseEntity<List<FolderTreeWrapper>> getFolderTreeByOwner(@PathVariable("ownerId") Long ownerId) {
        return new ResponseEntity<>(folderTreeService.getFolderTreeByOwner(ownerId), HttpStatus.OK);
    }

    @RequestMapping(value = ADD, method = RequestMethod.POST, consumes = "application/json;charset=utf-8")
    public ResponseEntity createFolderTree(@RequestBody FolderTree folderTree) {
        folderTreeService.createFolderTree(folderTree);
        return new ResponseEntity(HttpStatus.CREATED);
    }

    @RequestMapping(value = GET, method = RequestMethod.GET, produces = "application/json;charset=utf-8")
    @ResponseBody
    public ResponseEntity<FolderTree> retrieveFolderTree(@PathVariable("id") Long id) {
        return new ResponseEntity<>(folderTreeService.retrieveFolderTree(id), HttpStatus.OK);
    }

    @RequestMapping(value = UPDATE, method = RequestMethod.PUT, consumes = "application/json;charset=utf-8")
    public ResponseEntity updateFolderTree(@RequestBody FolderTree folderTree) {
        HttpStatus status = UserAccessUtility.checkAccess(folderTree.getOwner().getId());
        if (HttpStatus.OK.equals(status)) {
            folderTreeService.updateFolderTree(folderTree);
            return new ResponseEntity(status);
        } else {
            return new ResponseEntity(status);
        }
    }

    @RequestMapping(value = DELETE, method = RequestMethod.DELETE)
    public ResponseEntity deleteFolderTree(@PathVariable("id") Long id) {
        HttpStatus status = UserAccessUtility.checkAccess(id);
        if (HttpStatus.OK.equals(status)) {
            folderTreeService.deleteFolderTree(id);
            return new ResponseEntity(status);
        } else {
            return new ResponseEntity(status);
        }
    }

}
