package ru.tsc.task091017.rest.exceptionHandler;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;

/**
 * Created by aalbutov on 18.10.2017.
 */
@ControllerAdvice
public class ApiExceptionHandler {
    @ExceptionHandler(value = { AccessDeniedException.class ,ApiException.class,
            ConstraintViolationException.class, DataIntegrityViolationException.class,
            IllegalArgumentException.class, IllegalStateException.class})
    @ResponseBody
    protected ResponseEntity handleConflict(RuntimeException ex, WebRequest request) {
        HttpStatus status = HttpStatus.BAD_REQUEST;
        if (ex instanceof AccessDeniedException)status = HttpStatus.UNAUTHORIZED;
        if ((ex instanceof DataIntegrityViolationException)
                ||(ex instanceof ConstraintViolationException)
                ||(ex instanceof IllegalArgumentException)
                ||(ex instanceof IllegalStateException))status = HttpStatus.CONFLICT;
        if (ex instanceof ApiException)status = ((ApiException) ex).getKind();
        return ResponseEntity.status(status).contentType(MediaType.APPLICATION_JSON_UTF8).body(ex);    }
}
