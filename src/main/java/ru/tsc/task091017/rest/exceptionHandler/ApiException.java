package ru.tsc.task091017.rest.exceptionHandler;

import org.springframework.http.HttpStatus;

/**
 * Created by aalbutov on 18.10.2017.
 */
public class ApiException extends RuntimeException{
    public void setKind(HttpStatus kind) {
        this.kind = kind;
    }

    public HttpStatus getKind() {
        return kind;
    }

    private HttpStatus kind;

}
