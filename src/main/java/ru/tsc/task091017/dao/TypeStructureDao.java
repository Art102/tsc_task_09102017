package ru.tsc.task091017.dao;

import org.springframework.stereotype.Repository;
import ru.tsc.task091017.dao.domain.TypeStructure;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository
public class TypeStructureDao {

    @PersistenceContext
    private EntityManager em;

    public void createTypeStructure(TypeStructure typeStructure) {
        em.persist(typeStructure);
    }

    public TypeStructure retrieveTypeStructure(Long id) {
        return em.find(TypeStructure.class, id);
    }

    public void updateTypeStructure(TypeStructure typeStructure) {
        em.merge(typeStructure);
    }

    public void deleteTypeStructure(Long id) {
        TypeStructure typeStructure = em.find(TypeStructure.class, id);
        em.remove(typeStructure);
    }

}
