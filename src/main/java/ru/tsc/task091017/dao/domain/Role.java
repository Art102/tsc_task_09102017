package ru.tsc.task091017.dao.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * Created by aalbutov on 09.10.2017.
 */

@Entity
@Table(name="Role")
@XmlRootElement(name = "Role")
public class Role {
    @Id
    @Column(name = "id", nullable = false, columnDefinition = "NUMBER(2)"
    )
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter @Setter private Long id;

    @Column(name = "name", columnDefinition = "NVARCHAR2(200)"
    )
    @Getter @Setter private String name;


    @JsonIgnore
    @OneToMany(targetEntity = Users.class, orphanRemoval = false,
            cascade = CascadeType.MERGE, mappedBy="role")
    @Getter
    @Setter
    private List<Users> usersList;

    public Role(String name) {
        this.name = name;
    }

    public Role() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Role role = (Role) o;

        return name != null ? name.equals(role.name) : role.name == null;
    }

    @Override
    public int hashCode() {
        return name != null ? name.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "Role{" +
                "name='" + name + '\'' +
                '}';
    }
}
