package ru.tsc.task091017.dao.domain;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

public class FolderTreeWrapper extends FolderTree {
    @Getter
    @Setter
    private List<FolderTreeWrapper> subfolders = new ArrayList<>();

    public void add(FolderTreeWrapper w) {
        subfolders.add(w);
    }

    public FolderTreeWrapper(FolderTree f) {
        super(f.getId(), f.getParentId(), f.getName(), f.getToggled(), f.getDomain(), f.getOwner(), f.getCreated());
    }
}
