package ru.tsc.task091017.dao.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import ru.tsc.task091017.dao.domain.Domains;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.sql.Timestamp;
import java.util.List;

/**
 * Created by aalbutov on 09.10.2017.
 */
@Entity
@Table(name = "FolderTree")
@XmlRootElement(name = "FolderTree")
public class FolderTree {
    @Id
    @Column(name = "id", nullable = false, columnDefinition = "NVARCHAR2(36)"
    )
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    @Setter
    private Long id;

    @ManyToOne(targetEntity = FolderTree.class, cascade = CascadeType.MERGE)
    @JoinColumn(name = "parentId", columnDefinition = "NVARCHAR2(36)"
    )
    @Getter
    @Setter
    private FolderTree parentId;

    @Column(name = "name", columnDefinition = "NVARCHAR2(1000)"
    )
    @Getter
    @Setter
    private String name;

    @Column(name = "toggled", columnDefinition = "NUMBER(1)"
    )
    @Getter
    @Setter
    private int toggled;

    @ManyToOne(targetEntity = Domains.class)
    @JoinColumn(name = "domain", columnDefinition = "NVARCHAR2(200)"
    )
    @Getter
    @Setter
    private Domains domain;

    @ManyToOne(targetEntity = Users.class, cascade = CascadeType.MERGE)
    @JoinColumn(name = "owner", columnDefinition = "NVARCHAR2(200)"
    )
    @Getter
    @Setter
    private Users owner;

    @Column(name = "created", columnDefinition = "TIMESTAMP(6)")
    @Getter
    @Setter
    private Timestamp created;

    @JsonIgnore
    @ManyToMany(targetEntity = Document.class,
            cascade = CascadeType.MERGE, mappedBy="folderId")
    @Getter @Setter private List<Document> documentList;

    @JsonIgnore
    @OneToMany(targetEntity = FolderDocTypeRestriction.class,
            cascade = CascadeType.MERGE, mappedBy="folderId")
    @Getter @Setter private List<FolderDocTypeRestriction> folderDocTypeRestrictionList;

    @JsonIgnore
    @OneToMany(targetEntity = FolderTree.class, cascade = CascadeType.ALL, mappedBy="parentId")
    @Getter
    @Setter
    private List<FolderTree> childList;

    public FolderTree() {
    }

    public FolderTree(Long id, FolderTree parentId, String name, int toggled, Domains domain, Users owner, Timestamp created) {
        this.id = id;
        this.parentId = parentId;
        this.name = name;
        this.toggled = toggled;
        this.domain = domain;
        this.owner = owner;
        this.created = created;
    }

    public FolderTree(FolderTree parentId, String name, int toggled, Domains domain, Users owner, Timestamp created) {
        this.parentId = parentId;
        this.name = name;
        this.toggled = toggled;
        this.domain = domain;
        this.owner = owner;
        this.created = created;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FolderTree that = (FolderTree) o;

        if (toggled != that.toggled) return false;
        if (parentId != null ? !parentId.equals(that.parentId) : that.parentId != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (domain != null ? !domain.equals(that.domain) : that.domain != null) return false;
        if (owner != null ? !owner.equals(that.owner) : that.owner != null) return false;
        return created != null ? created.equals(that.created) : that.created == null;
    }

    @Override
    public int hashCode() {
        int result = parentId != null ? parentId.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + toggled;
        result = 31 * result + (domain != null ? domain.hashCode() : 0);
        result = 31 * result + (owner != null ? owner.hashCode() : 0);
        result = 31 * result + (created != null ? created.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "FolderTree{" +
                "parentId=" + parentId +
                ", name='" + name + '\'' +
                ", toggled=" + toggled +
                ", domain=" + domain +
                ", owner=" + owner +
                ", created=" + created +
                '}';
    }
}
