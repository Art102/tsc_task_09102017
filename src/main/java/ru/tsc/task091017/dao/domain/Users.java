package ru.tsc.task091017.dao.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.sql.Timestamp;
import java.util.List;

/**
 * Created by aalbutov on 09.10.2017.
 */

@Entity
@Table(name="Users")
@XmlRootElement(name = "User")
public class Users {

    @Id
    @Column(name = "id",nullable = false, columnDefinition = "NVARCHAR2(36)"
    )
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Getter @Setter private Long id;

    @ManyToOne(targetEntity = Domains.class, cascade = CascadeType.MERGE)
    @JoinColumn(name = "domain", columnDefinition = "NVARCHAR2(200)"
    )
    @Getter @Setter private Domains domain;

    @Column(name = "login", columnDefinition = "NVARCHAR2(200)"
    )
    @Getter @Setter private String login;

    //    @ManyToMany(targetEntity=Role.class, cascade = CascadeType.MERGE)
//    @JoinTable(
//            name="USER_ROLE",
//            joinColumns=@JoinColumn(name="USER", referencedColumnName="id"),
//            inverseJoinColumns=@JoinColumn(name="ROLE", referencedColumnName="Role.id"))
    @OneToOne(targetEntity = Role.class, cascade = CascadeType.MERGE)
    @JoinColumn(name = "role"//, columnDefinition = "NUMBER(2)"
    )
    @Getter @Setter private Role role;

    @Column(name = "created", columnDefinition = "TIMESTAMP(6)")
    @Getter @Setter private Timestamp created;

    @Column(name = "state", columnDefinition = "NUMBER(2)"
    )
    @Getter @Setter private int state;

    @JsonIgnore
    @OneToMany(targetEntity = FolderDocTypeRestriction.class, orphanRemoval = false,
            cascade = CascadeType.MERGE, mappedBy="owner")
    @Getter @Setter private List<FolderDocTypeRestriction> folderDocTypeRestrictionList;

    @JsonIgnore
    @OneToMany(targetEntity = FolderTree.class, orphanRemoval = false,
            cascade = CascadeType.MERGE, mappedBy="owner")
    @Getter @Setter private List<FolderTree> folderTreeList;

    @JsonIgnore
    @OneToMany(targetEntity = TypeStructure.class, orphanRemoval = false,
            cascade = CascadeType.MERGE, mappedBy="owner")
    @Getter @Setter private List<TypeStructure> typeStructureList;

    @JsonIgnore
    @OneToMany(targetEntity = Document.class, orphanRemoval = false,
            cascade = CascadeType.MERGE, mappedBy="owner")
    @Getter @Setter private List<Document> documentList;

    public Users(Domains domain, String login, Role role, Timestamp created, int state) {
        this.domain = domain;
        this.login = login;
        this.role = role;
        this.created = created;
        this.state = state;
    }

    public Users() {

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Users users = (Users) o;

        if (state != users.state) return false;
        if (domain != null ? !domain.equals(users.domain) : users.domain != null) return false;
        if (login != null ? !login.equals(users.login) : users.login != null) return false;
        if (role != null ? !role.equals(users.role) : users.role != null) return false;
        return created != null ? created.equals(users.created) : users.created == null;
    }

    @Override
    public int hashCode() {
        int result = domain != null ? domain.hashCode() : 0;
        result = 31 * result + (login != null ? login.hashCode() : 0);
        result = 31 * result + (role != null ? role.hashCode() : 0);
        result = 31 * result + (created != null ? created.hashCode() : 0);
        result = 31 * result + state;
        return result;
    }

    @Override
    public String toString() {
        return "Users{" +
                "domain=" + domain +
                ", login='" + login + '\'' +
                ", role=" + role +
                ", created=" + created +
                ", state=" + state +
                '}';
    }
}
