package ru.tsc.task091017.dao.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.sql.Timestamp;
import java.util.List;

/**
 * Created by aalbutov on 09.10.2017.
 */

@Entity
@Table(name = "Document")
@XmlRootElement(name = "Document")
public class Document {

    @Id
    @Column(name = "id",nullable = false, columnDefinition = "NVARCHAR2(36)"
    )
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Getter
    @Setter
    private Long id;

    @ManyToOne(targetEntity = Domains.class)
    @JoinColumn(name = "domain", columnDefinition = "NVARCHAR2(200)"
    )
    @Getter @Setter private Domains domain;

    @ManyToOne(targetEntity = Users.class)
    @JoinColumn(name = "owner", columnDefinition = "NVARCHAR2(200)"
    )
    @Getter @Setter private Users owner;

    @ManyToOne(targetEntity = TypeStructure.class, cascade = CascadeType.MERGE)
    @JoinColumn(name = "typeId", columnDefinition = "NVARCHAR2(36)"
    )
    @Getter @Setter private TypeStructure typeId;

    @ManyToMany(targetEntity=FolderTree.class, fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @JoinTable(
            name="DOCUMENT_FOLDER",
            joinColumns=@JoinColumn(name="DOCUMENT", referencedColumnName="id", columnDefinition = "NVARCHAR2(36)"
            ),
            inverseJoinColumns=@JoinColumn(name="FOLDER", referencedColumnName="id", columnDefinition = "NVARCHAR2(36)"
            ))
//    @Column(name = "folderId", columnDefinition = "NVARCHAR2(36)"
    @Getter @Setter private List<FolderTree> folderId;

    @Column(name = "title", columnDefinition = "NVARCHAR2(1000)"
    )
    @Getter @Setter private String title;

    @Column(name = "fieldValues", columnDefinition = "CLOB"
    )
    @Getter @Setter private String fieldValues;

    @Column(name = "created", columnDefinition = "TIMESTAMP(6)")
    @Getter @Setter private Timestamp created;

    @Column(name = "state", columnDefinition = "NVARCHAR2(36)"
    )

    @Getter @Setter private int state;

    public Document() {
    }

    public Document(Domains domain, Users owner, TypeStructure typeId, List<FolderTree> folderId, String title, String fieldValues, Timestamp created, int state) {
        this.domain = domain;
        this.owner = owner;
        this.typeId = typeId;
        this.folderId = folderId;
        this.title = title;
        this.fieldValues = fieldValues;
        this.created = created;
        this.state = state;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Document document = (Document) o;

        if (state != document.state) return false;
        if (domain != null ? !domain.equals(document.domain) : document.domain != null) return false;
        if (owner != null ? !owner.equals(document.owner) : document.owner != null) return false;
        if (typeId != null ? !typeId.equals(document.typeId) : document.typeId != null) return false;
        if (title != null ? !title.equals(document.title) : document.title != null) return false;
        if (fieldValues != null ? !fieldValues.equals(document.fieldValues) : document.fieldValues != null)
            return false;
        return created != null ? created.equals(document.created) : document.created == null;
    }

    @Override
    public int hashCode() {
        int result = domain != null ? domain.hashCode() : 0;
        result = 31 * result + (owner != null ? owner.hashCode() : 0);
        result = 31 * result + (typeId != null ? typeId.hashCode() : 0);
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (fieldValues != null ? fieldValues.hashCode() : 0);
        result = 31 * result + (created != null ? created.hashCode() : 0);
        result = 31 * result + state;
        return result;
    }

    @Override
    public String toString() {
        return "Document{" +
                "domain=" + domain +
                ", owner=" + owner +
                ", typeId=" + typeId +
                ", title='" + title + '\'' +
                ", fieldValues='" + fieldValues + '\'' +
                ", created=" + created +
                ", state=" + state +
                '}';
    }
}
