package ru.tsc.task091017.dao;

import org.springframework.stereotype.Repository;
import ru.tsc.task091017.dao.domain.Role;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Created by aalbutov on 12.10.2017.
 */
@Repository
public class RoleDao {

    @PersistenceContext
    private EntityManager em;

    public void createRole(Role role) {
        em.persist(role);
    }

    public Role retrieveRole(Long id) {
        return em.find(Role.class, id);
    }

    public void updateRole(Role role) {
        em.merge(role);
    }

    public void deleteRole(Long id) {
        Role role = em.find(Role.class, id);
        em.remove(role);
    }
}
