package ru.tsc.task091017.dao;

import org.springframework.stereotype.Repository;
import ru.tsc.task091017.dao.domain.FolderTree;
import ru.tsc.task091017.dao.domain.FolderTreeWrapper;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.List;

@Repository
public class FolderTreeDao {

    @PersistenceContext
    private EntityManager em;

    public FolderTreeWrapper getFolderTreeById(Long id) {
        FolderTree folder = em.find(FolderTree.class, id);

        FolderTreeWrapper folderTW = new FolderTreeWrapper(folder);

        linkFoldersRecursive(folderTW);

        return folderTW;
    }

    private void linkFoldersRecursive(FolderTreeWrapper folderTW) {
        List<FolderTree> folders = em.createQuery("SELECT f FROM FolderTree f WHERE f.parentId.id = :folderId",
                FolderTree.class)
                .setParameter("folderId", folderTW.getId())
                .getResultList();

        if (folders.isEmpty()) {
            return;
        }

        List<FolderTreeWrapper> foldersW = new ArrayList<>();
        for (FolderTree ft : folders) {
            foldersW.add(new FolderTreeWrapper(ft));
        }

        folderTW.setSubfolders(foldersW);

        for (FolderTreeWrapper ft : foldersW) {
            linkFoldersRecursive(ft);
        }
    }

    //возвращаем список корневых папок
    public List<FolderTreeWrapper> getFolderTreeByOwner(Long ownerId) {
        List<FolderTree> folders = em.createQuery("SELECT f FROM FolderTree f " +
                "WHERE f.owner.id = :ownerId", FolderTree.class)
                .setParameter("ownerId", ownerId)
                .getResultList();

        List<FolderTreeWrapper> folderTW = new ArrayList<>();
        for (FolderTree ft : folders) {
            folderTW.add(new FolderTreeWrapper(ft));
        }

        List<FolderTreeWrapper> rootFolders = new ArrayList<>();
        for (int i = 0; i < folderTW.size(); i++) {

            if (folderTW.get(i).getParentId() == null) {
                rootFolders.add(folderTW.get(i));
            }
            //связываем папки
            for (int j = i + 1; j < folderTW.size(); j++) {
                FolderTreeWrapper folder1 = folderTW.get(i);
                FolderTreeWrapper folder2 = folderTW.get(j);
                if (folder1.getParentId() != null && folder1.getParentId().getId().equals(folder2.getId())) {
                    folder2.add(folder1);
                } else if (folder2.getParentId() != null && folder2.getParentId().getId().equals(folder1.getId())) {
                    folder1.add(folder2);
                }
            }
        }

        return rootFolders;
    }

    public void createFolderTree(FolderTree folderTree) {
        em.persist(folderTree);
    }

    public FolderTree retrieveFolderTree(Long id) {
        return em.find(FolderTree.class, id);
    }

    public void updateFolderTree(FolderTree folderTree) {
        em.merge(folderTree);
    }

    public void deleteFolderTree(Long id) {
        FolderTree folderTree = em.find(FolderTree.class, id);
        em.remove(folderTree);
    }

}
