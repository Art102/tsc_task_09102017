package ru.tsc.task091017.dao;

import org.springframework.stereotype.Repository;
import ru.tsc.task091017.dao.domain.Domains;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository
public class DomainsDao {

    @PersistenceContext
    private EntityManager em;

    public void createDomains(Domains domains) {
        em.persist(domains);
    }

    public Domains retrieveDomains(Long id) {
        return em.find(Domains.class, id);
    }

    public void updateDomains(Domains domains) {
        em.merge(domains);
    }

    public void deleteDomains(Long id) {
        Domains domains = em.find(Domains.class, id);
        em.remove(domains);
    }

}
