package ru.tsc.task091017.dao.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.task091017.dao.domain.FolderDocTypeRestriction;
import ru.tsc.task091017.dao.FolderDocTypeRestrictionDao;

@Service
@Transactional
public class FolderDocTypeRestrictionService {

    private final FolderDocTypeRestrictionDao folderDocTypeRestrictionDao;

    @Autowired
    public FolderDocTypeRestrictionService(FolderDocTypeRestrictionDao folderDocTypeRestrictionDao) {
        this.folderDocTypeRestrictionDao = folderDocTypeRestrictionDao;
    }

    public void createFolderDocTypeRestriction(FolderDocTypeRestriction folderDocTypeRestriction) {
        folderDocTypeRestrictionDao.createFolderDocTypeRestriction(folderDocTypeRestriction);
    }

    public FolderDocTypeRestriction retrieveFolderDocTypeRestriction(Long id) {
        return folderDocTypeRestrictionDao.retrieveFolderDocTypeRestriction(id);
    }

    public void updateFolderDocTypeRestriction(FolderDocTypeRestriction folderDocTypeRestriction) {
        folderDocTypeRestrictionDao.updateFolderDocTypeRestriction(folderDocTypeRestriction);
    }

    public void deleteFolderDocTypeRestriction(Long id) {
        folderDocTypeRestrictionDao.deleteFolderDocTypeRestriction(id);
    }
}
