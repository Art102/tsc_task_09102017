package ru.tsc.task091017.dao.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.task091017.dao.FolderTreeDao;
import ru.tsc.task091017.dao.domain.FolderTree;
import ru.tsc.task091017.dao.domain.FolderTreeWrapper;

import java.util.List;

@Service
@Transactional
public class FolderTreeService {

    private final FolderTreeDao folderTreeDao;

    @Autowired
    public FolderTreeService(FolderTreeDao folderTreeDao) {
        this.folderTreeDao = folderTreeDao;
    }

    public FolderTreeWrapper getFolderTreeById(Long id) {
        return folderTreeDao.getFolderTreeById(id);
    }

    public List<FolderTreeWrapper> getFolderTreeByOwner(Long ownerId) {
        return folderTreeDao.getFolderTreeByOwner(ownerId);
    }

    public void createFolderTree(FolderTree folderTree) {
        folderTreeDao.createFolderTree(folderTree);
    }

    public FolderTree retrieveFolderTree(Long id) {
        return folderTreeDao.retrieveFolderTree(id);
    }

    public void updateFolderTree(FolderTree folderTree) {
        folderTreeDao.updateFolderTree(folderTree);
    }

    public void deleteFolderTree(Long id) {
        folderTreeDao.deleteFolderTree(id);
    }

}
