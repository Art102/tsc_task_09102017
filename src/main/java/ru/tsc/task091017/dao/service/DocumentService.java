package ru.tsc.task091017.dao.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.task091017.dao.DocumentDao;
import ru.tsc.task091017.dao.domain.Document;
import ru.tsc.task091017.dao.domain.TypeStructure;

import java.util.List;

@Service
@Transactional
public class DocumentService {

    private final DocumentDao documentDao;

    @Autowired
    public DocumentService(DocumentDao documentDao) {
        this.documentDao = documentDao;
    }

    public List<TypeStructure> getDocumentTypesByOwner(Long ownerId) {
        return documentDao.getDocumentTypesByOwner(ownerId);
    }

    public void createDocument(Document document) {
        documentDao.createDocument(document);
    }

    public Document retrieveDocument(Long id) {
        return documentDao.retrieveDocument(id);
    }

    public void updateDocument(Document document) {
        documentDao.updateDocument(document);
    }

    public void deleteDocument(Long id) {
        documentDao.deleteDocument(id);
    }

}
