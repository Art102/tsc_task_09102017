package ru.tsc.task091017.dao.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.task091017.dao.domain.TypeStructure;
import ru.tsc.task091017.dao.TypeStructureDao;

@Service
@Transactional
public class TypeStructureService {

    private final TypeStructureDao typeStructureDao;

    @Autowired
    public TypeStructureService(TypeStructureDao typeStructureDao) {
        this.typeStructureDao = typeStructureDao;
    }

    public void createTypeStructure(TypeStructure typeStructure) {
        typeStructureDao.createTypeStructure(typeStructure);
    }

    public TypeStructure retrieveTypeStructure(Long id) {
        return typeStructureDao.retrieveTypeStructure(id);
    }

    public void updateTypeStructure(TypeStructure typeStructure) {
        typeStructureDao.updateTypeStructure(typeStructure);
    }

    public void deleteTypeStructure(Long id) {
        typeStructureDao.deleteTypeStructure(id);
    }
}
