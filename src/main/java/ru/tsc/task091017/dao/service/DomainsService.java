package ru.tsc.task091017.dao.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.task091017.dao.DomainsDao;
import ru.tsc.task091017.dao.domain.Domains;

@Service
@Transactional
public class DomainsService {

    private final DomainsDao domainsDao;

    @Autowired
    public DomainsService(DomainsDao domainsDao) {
        this.domainsDao = domainsDao;
    }

    public void createDomains(Domains domains) {
        domainsDao.createDomains(domains);
    }

    public Domains retrieveDomains(Long id) {
        return domainsDao.retrieveDomains(id);
    }

    public void updateDomains(Domains domains) {
        domainsDao.updateDomains(domains);
    }

    public void deleteDomains(Long id) {
        domainsDao.deleteDomains(id);
    }

}
