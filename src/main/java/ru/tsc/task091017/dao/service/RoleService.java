package ru.tsc.task091017.dao.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.task091017.dao.RoleDao;
import ru.tsc.task091017.dao.domain.Role;

/**
 * Created by aalbutov on 12.10.2017.
 */
@Service
@Transactional
public class RoleService {

    private final RoleDao roleDao;

    @Autowired
    public RoleService(RoleDao roleDao) {
        this.roleDao = roleDao;
    }

    public void createRole(Role role) {
        roleDao.createRole(role);
    }

    public Role retrieveRole(Long id) {
        return roleDao.retrieveRole(id);
    }

    public void updateRole(Role role) {
        roleDao.updateRole(role);
    }

    public void deleteRole(Long id) {
        roleDao.deleteRole(id);
    }
}
