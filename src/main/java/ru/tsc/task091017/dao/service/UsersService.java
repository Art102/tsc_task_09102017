package ru.tsc.task091017.dao.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.task091017.dao.UsersDao;
import ru.tsc.task091017.dao.domain.Users;

@Service
@Transactional
public class UsersService {

    private final UsersDao usersDao;

    @Autowired
    public UsersService(UsersDao usersDao) {
        this.usersDao = usersDao;
    }

    public void createUsers(Users users) {
        usersDao.createUsers(users);
    }

    public Users retrieveUsers(Long id) {
        return usersDao.retrieveUsers(id);
    }

    public void updateUsers(Users users) {
        usersDao.updateUsers(users);
    }

    public void deleteUsers(Long id) {
        usersDao.deleteUsers(id);
    }

    public Users getUserByLogin(String login) {
        return usersDao.getUserByLogin(login);
    }

}
