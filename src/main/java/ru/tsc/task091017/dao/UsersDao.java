package ru.tsc.task091017.dao;

import org.springframework.stereotype.Repository;
import ru.tsc.task091017.dao.domain.Users;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository
public class UsersDao {

    @PersistenceContext
    private EntityManager em;

    public void createUsers(Users users) {
        em.persist(users);
    }

    public Users retrieveUsers(Long id) {
        return em.find(Users.class, id);
    }

    public void updateUsers(Users users) {
        em.merge(users);
    }

    public void deleteUsers(Long id) {
        Users users = em.find(Users.class, id);
        em.remove(users);
    }

    public Users getUserByLogin(String login) {
        return em.createQuery("SELECT u FROM Users u WHERE u.login = :login", Users.class)
                .setParameter("login", login)
                .getSingleResult();
    }

}
