package ru.tsc.task091017.dao;

import org.springframework.stereotype.Repository;
import ru.tsc.task091017.dao.domain.TypeStructure;
import ru.tsc.task091017.dao.domain.Document;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class DocumentDao {

    @PersistenceContext
    private EntityManager em;

    public List<TypeStructure> getDocumentTypesByOwner(Long ownerId) {
        return em.createQuery("SELECT d.typeId FROM Document d WHERE d.owner.id = :ownerId", TypeStructure.class)
                .setParameter("ownerId", ownerId)
                .getResultList();
    }

    public void createDocument(Document document) {
        em.persist(document);
    }

    public Document retrieveDocument(Long id) {
        return em.find(Document.class, id);
    }

    public void updateDocument(Document document) {
        em.merge(document);
    }

    public void deleteDocument(Long id) {
        Document document = em.find(Document.class, id);
        em.remove(document);
    }

}
