package ru.tsc.task091017.dao;

import org.springframework.stereotype.Repository;
import ru.tsc.task091017.dao.domain.FolderDocTypeRestriction;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository
public class FolderDocTypeRestrictionDao {

    @PersistenceContext
    private EntityManager em;

    public void createFolderDocTypeRestriction(FolderDocTypeRestriction folderDocTypeRestriction) {
        em.persist(folderDocTypeRestriction);
    }

    public FolderDocTypeRestriction retrieveFolderDocTypeRestriction(Long id) {
        return em.find(FolderDocTypeRestriction.class, id);
    }

    public void updateFolderDocTypeRestriction(FolderDocTypeRestriction folderDocTypeRestriction) {
        em.merge(folderDocTypeRestriction);
    }

    public void deleteFolderDocTypeRestriction(Long id) {
        FolderDocTypeRestriction folderDocTypeRestriction = em.find(FolderDocTypeRestriction.class, id);
        em.remove(folderDocTypeRestriction);
    }

}
