CREATE TABLE document
(
  id          BIGINT AUTO_INCREMENT
  PRIMARY KEY,
  created     TIMESTAMP(6) DEFAULT CURRENT_TIMESTAMP(6) NOT NULL,
  fieldValues VARCHAR(255)                              NULL,
  state       INT                                       NULL,
  title       VARCHAR(255)                              NULL,
  domain      BIGINT                                    NULL,
  owner       BIGINT                                    NULL,
  typeId      BIGINT                                    NULL
);
CREATE INDEX FK938a75ud24ylcwu8mq3nflj9g
  ON document (domain);
CREATE INDEX FKnwrdou6wtdlkut2yddts6jsel
  ON document (typeId);
CREATE INDEX FKq213efp31xtk7lwn5vw6wfosn
  ON document (owner);

CREATE TABLE document_folder
(
  DOCUMENT BIGINT NOT NULL,
  FOLDER   BIGINT NOT NULL,
  CONSTRAINT UK_2o26e7mawj6vn33ogawy2anhw
  UNIQUE (FOLDER)
);
CREATE INDEX FKevveyavw6k9gemrx20cnfwa54
  ON document_folder (DOCUMENT);

CREATE TABLE domains
(
  id      BIGINT AUTO_INCREMENT
  PRIMARY KEY,
  created TIMESTAMP(6) DEFAULT CURRENT_TIMESTAMP(6) NOT NULL,
  domain  VARCHAR(255)                              NULL,
  state   INT                                       NULL
);

CREATE TABLE folderdoctyperestriction
(
  id       BIGINT AUTO_INCREMENT
  PRIMARY KEY,
  created  TIMESTAMP(6) DEFAULT CURRENT_TIMESTAMP(6) NOT NULL,
  domain   BIGINT                                    NULL,
  folderId BIGINT                                    NULL,
  owner    BIGINT                                    NULL,
  typeId   BIGINT                                    NULL
);
CREATE INDEX FKax3t98snkbavblb0qknkfuo4r
  ON folderdoctyperestriction (folderId);
CREATE INDEX FKjwr7wte2hj4k838sfs7ofudmy
  ON folderdoctyperestriction (typeId);
CREATE INDEX FKquiob5k6polqm5vto216lu0yy
  ON folderdoctyperestriction (owner);
CREATE INDEX FKsa0ajye6tcv1op40m93mb0k1
  ON folderdoctyperestriction (domain);

CREATE TABLE foldertree
(
  id       BIGINT AUTO_INCREMENT
  PRIMARY KEY,
  created  TIMESTAMP(6) DEFAULT CURRENT_TIMESTAMP(6) NOT NULL,
  name     VARCHAR(255)                              NULL,
  toggled  INT                                       NULL,
  domain   BIGINT                                    NULL,
  owner    BIGINT                                    NULL,
  parentId BIGINT                                    NULL
);
CREATE INDEX FK6gwrn5hvcobn2me06umt9ngrg
  ON foldertree (domain);
CREATE INDEX FK8sd361kxrruyv88eh74dw37y1
  ON foldertree (owner);
CREATE INDEX FKlm72cl5xabrv29maxtkai5411
  ON foldertree (parentId);

CREATE TABLE role
(
  id   BIGINT AUTO_INCREMENT
  PRIMARY KEY,
  name VARCHAR(255) NULL
);

CREATE TABLE typestructure
(
  id        BIGINT AUTO_INCREMENT
  PRIMARY KEY,
  created   TIMESTAMP(6) DEFAULT CURRENT_TIMESTAMP(6) NOT NULL,
  Name      VARCHAR(255)                              NULL,
  state     INT                                       NULL,
  Structure VARCHAR(255)                              NULL,
  domain    BIGINT                                    NULL,
  owner     BIGINT                                    NULL
);
CREATE INDEX FK2qwwreyjos509xejaqdmjycwq
  ON typestructure (domain);
CREATE INDEX FKmtpwhm23kttiip3rxfx4gqq0
  ON typestructure (owner);

CREATE TABLE users
(
  id      BIGINT AUTO_INCREMENT
  PRIMARY KEY,
  created TIMESTAMP(6) DEFAULT CURRENT_TIMESTAMP(6) NOT NULL,
  login   VARCHAR(255)                              NULL,
  state   INT                                       NULL,
  domain  BIGINT                                    NULL,
  role    BIGINT                                    NULL
);
CREATE INDEX FK7ooeu91bpjgvo0kquawsvtghm
  ON users (role);
CREATE INDEX FKhda9j1kcf52awpiijvheq4rum
  ON users (domain);