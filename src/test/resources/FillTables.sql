INSERT INTO domains (created, domain, state) VALUES ('1970-01-02 06:46:40.0', 'domain1', 1);

INSERT INTO role (name) VALUES ('admin');

INSERT INTO users (created, login, state, domain, role) VALUES ('1970-01-02 06:46:40.0', 'login1', 1, 1, 1);

INSERT INTO document (created, fieldValues, state, title, domain, owner, typeId)
VALUES ('1970-01-02 06:46:40.0', 'fieldValues1', 1, 'doc1', 1, 1, 1);

INSERT INTO typestructure (created, Name, state, Structure, domain, owner)
VALUES ('1970-01-02 06:46:40.0', 'type1', 1, 'structure1', 1, 1);

INSERT INTO foldertree (created, name, toggled, domain, owner, parentId)
VALUES ('1970-01-02 06:46:40.0', 'folder1', 1, 1, 1, NULL);
INSERT INTO foldertree (created, name, toggled, domain, owner, parentId)
VALUES ('1970-01-02 06:46:40.0', 'folder11', 1, 1, 1, 1);
INSERT INTO foldertree (created, name, toggled, domain, owner, parentId)
VALUES ('1970-01-02 06:46:40.0', 'folder12', 1, 1, 1, 1);
INSERT INTO foldertree (created, name, toggled, domain, owner, parentId)
VALUES ('1970-01-02 06:46:40.0', 'folder111', 1, 1, 1, 2);
INSERT INTO foldertree (created, name, toggled, domain, owner, parentId)
VALUES ('1970-01-02 06:46:40.0', 'folder112', 1, 1, 1, 2);

INSERT INTO folderdoctyperestriction (created, domain, folderId, owner, typeId)
VALUES ('1970-01-02 06:46:40.0', 1, 1, 1, 1);

INSERT INTO document_folder (DOCUMENT, FOLDER) VALUES (1, 1);

