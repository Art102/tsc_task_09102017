package ru.tsc.task091017.rest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.tsc.task091017.config.Config;
import ru.tsc.task091017.config.JpaConfig;
import ru.tsc.task091017.config.SecurityConfig;
import ru.tsc.task091017.config.SpringWebConfig;
import ru.tsc.task091017.dao.service.*;

import java.io.IOException;
import java.nio.charset.Charset;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Date;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {Config.class, JpaConfig.class, SecurityConfig.class, SpringWebConfig.class})
@WebAppConfiguration
@ActiveProfiles("test")
public class CrudControllersTestDisabled {
    private static final String DOCUMENT_MAPPING= "/rest/Document";
    private static final String DOMAINS_MAPPING= "/rest/Domains";
    private static final String FOLDER_DOC_TYPE_RESTRICTION_MAPPING= "/rest/FolderDocTypeRestriction";
    private static final String FOLDER_TREE_MAPPING= "/rest/FolderTree";
    private static final String TYPE_STRUCTURE_MAPPING= "/rest/TypeStructure";
    private static final String USERS_MAPPING= "/rest/Users";

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private RoleService roleService;
    @Autowired
    private UsersService usersService;
    @Autowired
    private DocumentService documentService;
    @Autowired
    private TypeStructureService typeStructureService;
    @Autowired
    private FolderDocTypeRestrictionService folderDocTypeRestrictionService;
    @Autowired
    private FolderTreeService folderTreeService;
    @Autowired
    private DomainsService domainsService;

    private MockMvc mockMvc;

    private HttpMessageConverter mappingJackson2HttpMessageConverter;

    private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            Charset.forName("utf8"));

    private static int inited = 0;

    private  static ru.tsc.task091017.dao.domain.Document document;
    private  static ru.tsc.task091017.dao.domain.Domains domains;
    private  static ru.tsc.task091017.dao.domain.FolderDocTypeRestriction folderDocTypeRestriction;
    private  static ru.tsc.task091017.dao.domain.FolderTree folderTree;
    private  static ru.tsc.task091017.dao.domain.Role role;
    private  static ru.tsc.task091017.dao.domain.TypeStructure typeStructure;
    private  static ru.tsc.task091017.dao.domain.Users users;
    private  static ru.tsc.task091017.dao.domain.Document document3;
    private  static ru.tsc.task091017.dao.domain.Domains domains3;
    private  static ru.tsc.task091017.dao.domain.FolderDocTypeRestriction folderDocTypeRestriction3;
    private  static ru.tsc.task091017.dao.domain.FolderTree folderTree3;
    private  static ru.tsc.task091017.dao.domain.Role role3;
    private  static ru.tsc.task091017.dao.domain.TypeStructure typeStructure3;
    private  static ru.tsc.task091017.dao.domain.Users users3;


    private static Timestamp created;
    private static int state;
    private static int toggled;
    private static HttpHeaders httpHeaders;

    @Before
    public void setup() {
        mappingJackson2HttpMessageConverter = new MappingJackson2HttpMessageConverter();
        created= new Timestamp(new Date().getTime());
        state = 1;
        toggled = 2;
        if (inited<=0){
            role = new ru.tsc.task091017.dao.domain.Role("role1");
            roleService.createRole(role);
            domains = new ru.tsc.task091017.dao.domain.Domains("domain1",created, 1);
            domainsService.createDomains(domains);
            users = new ru.tsc.task091017.dao.domain.Users(domains, "user1", role, created, state);
            usersService.createUsers(users);
            typeStructure = new ru.tsc.task091017.dao.domain.TypeStructure("type1", "structure", domains, users, created, state);
            typeStructureService.createTypeStructure(typeStructure);
            folderTree = new ru.tsc.task091017.dao.domain.FolderTree(null, "folder1", toggled, domains, users, created);
            folderTreeService.createFolderTree(folderTree);
            folderDocTypeRestriction = new ru.tsc.task091017.dao.domain.FolderDocTypeRestriction(domains, users, typeStructure, folderTree, created);
            folderDocTypeRestrictionService.createFolderDocTypeRestriction(folderDocTypeRestriction);
            document = new ru.tsc.task091017.dao.domain.Document(domains, users, typeStructure, Arrays.asList(folderTree), "title", "fieldValues", created, state);
            documentService.createDocument(document);
            role3 = new ru.tsc.task091017.dao.domain.Role("role3");
            roleService.createRole(role3);
            domains3 = new ru.tsc.task091017.dao.domain.Domains("domain3",created, 1);
            domainsService.createDomains(domains3);
            users3 = new ru.tsc.task091017.dao.domain.Users(domains, "user3", role3, created, state);
            usersService.createUsers(users3);
            typeStructure3 = new ru.tsc.task091017.dao.domain.TypeStructure("type3", "structure", domains3, users3, created, state);
            typeStructureService.createTypeStructure(typeStructure3);
            folderTree3 = new ru.tsc.task091017.dao.domain.FolderTree(null, "folder3", toggled, domains3, users3, created);
            folderTreeService.createFolderTree(folderTree3);
            folderDocTypeRestriction3 = new ru.tsc.task091017.dao.domain.FolderDocTypeRestriction(domains3, users3, typeStructure3, folderTree3, created);
            folderDocTypeRestrictionService.createFolderDocTypeRestriction(folderDocTypeRestriction3);
            document3 = new ru.tsc.task091017.dao.domain.Document(domains3, users3, typeStructure3, Arrays.asList(folderTree3), "title3", "fieldValues3", created, state);
            documentService.createDocument(document3);
            inited++;
        }

        mockMvc = MockMvcBuilders.webAppContextSetup(context)
//                .apply(springSecurity())
                .build();
        httpHeaders = new HttpHeaders();
        httpHeaders.add("X-AUTH-TOKEN", "user");
    }

    private String json(Object o) throws IOException {
        MockHttpOutputMessage mockHttpOutputMessage = new MockHttpOutputMessage();
        this.mappingJackson2HttpMessageConverter.write(
                o, MediaType.APPLICATION_JSON, mockHttpOutputMessage);
        return mockHttpOutputMessage.getBodyAsString();
    }

    // Create tests

    @Test
    public void createDomainsTest() throws Exception{
        ru.tsc.task091017.dao.domain.Domains domains2 = new ru.tsc.task091017.dao.domain.Domains("domain2",created, 1);
        String domainsJson = json(domains2);
        this.mockMvc.perform(post(DOMAINS_MAPPING+DomainsController.ADD)
                .contentType(contentType)
                .headers(httpHeaders)
                .content(domainsJson))
                .andExpect(status().isCreated());
    }

    @Test
    public void createUsersTest() throws Exception{
        ru.tsc.task091017.dao.domain.Users users2 = new ru.tsc.task091017.dao.domain.Users(domains, "user2", role, created, state);
        String usersJson = json(users2);
        this.mockMvc.perform(post(USERS_MAPPING+UsersController.ADD)
                .headers(httpHeaders)
                .contentType(contentType)
                .content(usersJson))
                .andExpect(status().isCreated());
    }

    @Test
    public void createTypeStructureTest() throws Exception{
        ru.tsc.task091017.dao.domain.TypeStructure typeStructure2 = new ru.tsc.task091017.dao.domain.TypeStructure("type2", "structure2", domains, users, created, state);
        String typeStructureJson = json(typeStructure2);
        this.mockMvc.perform(post(TYPE_STRUCTURE_MAPPING+TypeStructureController.ADD)
                .headers(httpHeaders)
                .contentType(contentType)
                .content(typeStructureJson))
                .andExpect(status().isCreated());
    }

    @Test
    public void createFolderTreeTest() throws Exception{
        ru.tsc.task091017.dao.domain.FolderTree folderTree2 = new ru.tsc.task091017.dao.domain.FolderTree(folderTree, "folder2", toggled, domains, users, created);
        String folderTreeJson = json(folderTree2);
        this.mockMvc.perform(post(FOLDER_TREE_MAPPING+
                FolderTreeController.ADD)
                .headers(httpHeaders)
                .contentType(contentType)
                .content(folderTreeJson))
                .andExpect(status().isCreated());
    }

    @Test
    public void createDocumentTest() throws Exception{
        ru.tsc.task091017.dao.domain.Document document2 = new ru.tsc.task091017.dao.domain.Document(domains, users, typeStructure, Arrays.asList(folderTree), "title2", "fieldValues2", created, state);

        String documentJson = json(document2);
        this.mockMvc.perform(post(DOCUMENT_MAPPING+DocumentController.ADD)
                .headers(httpHeaders)
                .contentType(contentType)
                .content(documentJson))
                .andExpect(status().isCreated());
    }

    @Test
    public void createFolderDocTypeRestrictionTest() throws Exception{
        ru.tsc.task091017.dao.domain.FolderDocTypeRestriction folderDocTypeRestriction2 =
                new ru.tsc.task091017.dao.domain.FolderDocTypeRestriction(domains, users, typeStructure, folderTree, created);
        String folderDocTypeRestrictionJson = json(folderDocTypeRestriction2);
        this.mockMvc.perform(post(FOLDER_DOC_TYPE_RESTRICTION_MAPPING+
                FolderDocTypeRestrictionController.ADD)
                .headers(httpHeaders)
                .contentType(contentType)
                .content(folderDocTypeRestrictionJson))
                .andExpect(status().isCreated());
    }

    // Document REST CRUD tests

    @Test
    public void getDocumentTest() throws Exception{
        mockMvc.perform(get(DOCUMENT_MAPPING+DocumentController.GET.replace("{id}","1"))
                .contentType(contentType)
                .headers(httpHeaders))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.title", is(this.document.getTitle())));
    }

    @Test
    public void putDocumentTest() throws Exception{
        String documentJson = json(document);
        mockMvc.perform(put(DOCUMENT_MAPPING+DocumentController.UPDATE)
                .headers(httpHeaders)
                .contentType(contentType)
                .content(documentJson))
                .andExpect(status().isOk());
    }

    //  Document -> TypeStructure relation ManyToOne,
    // if TypeStructure deleted, Document can't exist
    // so it has Cascade.REMOVE

//    @Test
//    public void deleteDocumentTest() throws Exception{
//        mockMvc.perform(delete(DOCUMENT_MAPPING+DocumentController.DELETE.replace("{id}","2"))
//                .headers(httpHeaders))
//                .andExpect(status().isOk());
//    }

    @Test
    public void getDocumentsTypesByOwnerTest() throws Exception{
        mockMvc.perform(get(DOCUMENT_MAPPING+DocumentController.GET_TYPES_BY_OWNER.replace("{ownerId}","1"))
                .headers(httpHeaders))
                .andExpect(status().isOk())
                //todo нужно добавить проверок
                .andExpect(content().contentType(contentType));
    }

    @Test
    public void getDomainsTest() throws Exception{
        mockMvc.perform(get(DOMAINS_MAPPING+DomainsController.GET.replace("{id}","1"))
                .headers(httpHeaders))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.domain", is(this.domains.getDomain())));
    }

    @Test
    public void putDomainsTest() throws Exception{
        String domainsJson = json(domains);
        mockMvc.perform(put(DOMAINS_MAPPING+DomainsController.UPDATE)
                .headers(httpHeaders)
                .contentType(contentType)
                .content(domainsJson))
                .andExpect(status().isOk());
    }

//    @Test
//    public void deleteDomainsTest() throws Exception{
//        mockMvc.perform(delete(DOMAINS_MAPPING+DomainsController.DELETE.replace("{id}","2"))
//                .headers(httpHeaders))
//                .andExpect(status().isOk());
//    }

    // FolderDocTypeRestriction REST CRUD tests


    @Test
    public void getFolderDocTypeRestrictionTest() throws Exception{
        mockMvc.perform(get(FOLDER_DOC_TYPE_RESTRICTION_MAPPING+
                FolderDocTypeRestrictionController.GET.replace("{id}","1"))
                .contentType(contentType)
                .headers(httpHeaders))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.id", is(this.folderDocTypeRestriction.getId().intValue())));
    }

    @Test
    public void putFolderDocTypeRestrictionTest() throws Exception{
        String folderDocTypeRestrictionJson = json(folderDocTypeRestriction);
        mockMvc.perform(put(FOLDER_DOC_TYPE_RESTRICTION_MAPPING+
                FolderDocTypeRestrictionController.UPDATE)
                .headers(httpHeaders)
                .contentType(contentType)
                .content(folderDocTypeRestrictionJson))
                .andExpect(status().isOk());
    }

//    @Test
//    public void deleteFolderDocTypeRestrictionTest() throws Exception{
//        mockMvc.perform(delete(FOLDER_DOC_TYPE_RESTRICTION_MAPPING+
//                FolderDocTypeRestrictionController.DELETE.replace("{id}","2"))
//                .headers(httpHeaders))
//                .andExpect(status().isOk());
//    }


    // FolderTree REST CRUD tests


    @Test
    public void getFolderTreeTest() throws Exception{
        mockMvc.perform(get(FOLDER_TREE_MAPPING+
                FolderTreeController.GET.replace("{id}","1"))
                .contentType(contentType)
                .headers(httpHeaders))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.name", is(this.folderTree.getName())));
    }

    @Test
    public void putFolderTreeTest() throws Exception{
        String folderTreeJson = json(folderTree);
        mockMvc.perform(put(FOLDER_TREE_MAPPING+
                FolderTreeController.UPDATE)
                .headers(httpHeaders)
                .contentType(contentType)
                .content(folderTreeJson))
                .andExpect(status().isOk());
    }

//    @Test
//    public void deleteFolderTreeTest() throws Exception{
//        mockMvc.perform(delete(FOLDER_TREE_MAPPING+
//                FolderTreeController.DELETE.replace("{id}","2")))
//                .andExpect(status().isOk());
//    }

    @Test
    public void getFolderTreeByOwnerTest() throws Exception{
        mockMvc.perform(get(FOLDER_TREE_MAPPING+
                FolderTreeController.GET_BY_OWNER.replace("{ownerId}","1"))
                .headers(httpHeaders))
                .andExpect(status().isOk())
                //todo нужно добавить проверок
                .andExpect(content().contentType(contentType));
    }

    @Test
    public void getFolderTreeByFolderTest() throws Exception{
        mockMvc.perform(get(FOLDER_TREE_MAPPING+
                FolderTreeController.GET_BY_ID.replace("{id}","1"))
                .headers(httpHeaders))
                .andExpect(status().isOk())
                //todo нужно добавить проверок
                .andExpect(content().contentType(contentType));
    }


    // TypeStructure REST CRUD tests


    @Test
    public void getTypeStructureTest() throws Exception{
        mockMvc.perform(get(TYPE_STRUCTURE_MAPPING+TypeStructureController.GET.replace("{id}","1"))
                .contentType(contentType)
                .headers(httpHeaders))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.name", is(this.typeStructure.getName())));
    }

    @Test
    public void putTypeStructureTest() throws Exception{
        String typeStructureJson = json(typeStructure);
        mockMvc.perform(put(TYPE_STRUCTURE_MAPPING+TypeStructureController.UPDATE)
                .headers(httpHeaders)
                .contentType(contentType)
                .content(typeStructureJson))
                .andExpect(status().isOk());
    }

//    @Test
//    public void deleteTypeStructureTest() throws Exception{
//        mockMvc.perform(delete(TYPE_STRUCTURE_MAPPING+TypeStructureController.DELETE.replace("{id}","2"))
//                .headers(httpHeaders))
//                .andExpect(status().isOk());
//    }

    // Users REST CRUD tests


    @Test
    public void getUsersTest() throws Exception{
        mockMvc.perform(get(USERS_MAPPING+UsersController.GET.replace("{id}","1"))
                .contentType(contentType)
                .headers(httpHeaders))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.login", is(this.users.getLogin())));
    }

    @Test
    public void putUsersTest() throws Exception{
        String usersJson = json(users);
        mockMvc.perform(put(USERS_MAPPING+UsersController.UPDATE)
                .contentType(contentType)
                .headers(httpHeaders)
                .content(usersJson))
                .andExpect(status().isOk());
    }

//    @Test
//    public void deleteUsersTest() throws Exception{
//        mockMvc.perform(delete(USERS_MAPPING+UsersController.DELETE.replace("{id}","2"))
//                .headers(httpHeaders))
//                .andExpect(status().isOk());
//    }
}
