package ru.tsc.task091017.dao;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.task091017.dao.domain.FolderTree;
import ru.tsc.task091017.dao.domain.FolderTreeWrapper;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:applicationContextTest.xml")
public class FolderTreeDaoTest extends DaoTest {
    @Autowired
    private FolderTreeDao folderTreeDao;

    @SuppressWarnings("unchecked")
    private List<FolderTree> folderTrees = (List<FolderTree>) createLists().get(3);
    private List<FolderTreeWrapper> folderTreeWrappers = createFolderTreeWrappers();

    @Transactional
    @Test
    public void getFolderTreeTest() {
        FolderTree ft = folderTreeDao.retrieveFolderTree(1L);
        assertEquals(ft, folderTrees.get(0));
    }

    @Transactional
    @Test
    public void createFolderTreeTest() {
        FolderTree ft1 = folderTrees.get(0);
        ft1.setId(null);
        ft1.setName("newFolderTree");
        folderTreeDao.createFolderTree(ft1);
        assertEquals(folderTreeDao.retrieveFolderTree(6L), ft1);
    }

    @Transactional
    @Test
    public void updateFolderTreeTest() {
        FolderTree ft1 = folderTrees.get(0);
        ft1.setName("newFolderTree2");
        folderTreeDao.updateFolderTree(ft1);
        assertEquals(folderTreeDao.retrieveFolderTree(1L), ft1);
    }

    @Transactional
    @Test
    public void deleteFolderTreeTest() {
        folderTreeDao.deleteFolderTree(1L);
        assertNull(folderTreeDao.retrieveFolderTree(1L));
    }

    @Transactional
    @Test
    public void getFolderTreeByIdTest() {
        FolderTreeWrapper folderTreeWrapper = folderTreeDao.getFolderTreeById(1L);
        assertEquals(folderTreeWrapper, folderTreeWrappers.get(0));
    }

    @Transactional
    @Test
    public void getFolderTreeByOwnerTest() {
        List<FolderTreeWrapper> folderTreeWrapper = folderTreeDao.getFolderTreeByOwner(1L);
        if (folderTreeWrapper.size() == 1) {
            assertEquals(folderTreeWrapper.get(0), folderTreeWrappers.get(0));
        } else {
            assertNull("wrong size", folderTreeWrapper);
        }

    }

}
