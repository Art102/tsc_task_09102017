package ru.tsc.task091017.dao;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.task091017.dao.domain.FolderDocTypeRestriction;

import java.sql.Timestamp;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:applicationContextTest.xml")
public class FolderDocTypeRestrictionDaoTest extends DaoTest {
    @Autowired
    private FolderDocTypeRestrictionDao folderDTRDao;

    @SuppressWarnings("unchecked")
    private List<FolderDocTypeRestriction> folderDTRs = (List<FolderDocTypeRestriction>) createLists().get(2);

    @Transactional
    @Test
    public void getFolderDocTypeRestrictionTest() {
        FolderDocTypeRestriction ts = folderDTRDao.retrieveFolderDocTypeRestriction(1L);
        assertEquals(ts, folderDTRs.get(0));
    }

    @Transactional
    @Test
    public void createFolderDocTypeRestrictionTest() {
        FolderDocTypeRestriction ts1 = folderDTRs.get(0);
        ts1.setId(null);
        ts1.setCreated(new Timestamp(3000000));
        folderDTRDao.createFolderDocTypeRestriction(ts1);
        assertEquals(folderDTRDao.retrieveFolderDocTypeRestriction(2L), ts1);
    }

    @Transactional
    @Test
    public void updateFolderDocTypeRestrictionTest() {
        FolderDocTypeRestriction ts1 = folderDTRs.get(0);
        ts1.setCreated(new Timestamp(4000000));
        folderDTRDao.updateFolderDocTypeRestriction(ts1);
        assertEquals(folderDTRDao.retrieveFolderDocTypeRestriction(1L), ts1);
    }

    @Transactional
    @Test
    public void deleteFolderDocTypeRestrictionTest() {
        folderDTRDao.deleteFolderDocTypeRestriction(1L);
        assertNull(folderDTRDao.retrieveFolderDocTypeRestriction(1L));
    }

}
