package ru.tsc.task091017.dao;

import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@SqlGroup({
        @Sql(value = {"classpath:CreateTables.sql", "classpath:FillTables.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
        @Sql(value = "classpath:DropSchema.sql", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
})
public class DaoTest {

    protected static List<List<?>> createLists() {
        Timestamp ts = new Timestamp(100000000);
        ru.tsc.task091017.dao.domain.Domains domain = new ru.tsc.task091017.dao.domain.Domains();
        domain.setId(1L);
        domain.setCreated(ts);
        domain.setDomain("domain1");
        domain.setState(1);
        ru.tsc.task091017.dao.domain.Role role = new ru.tsc.task091017.dao.domain.Role();
        role.setId(1L);
        role.setName("admin");
        ru.tsc.task091017.dao.domain.Users user = new ru.tsc.task091017.dao.domain.Users();
        user.setId(1L);
        user.setCreated(ts);
        user.setLogin("login1");
        user.setState(1);
        user.setDomain(domain);
        user.setRole(role);
        ru.tsc.task091017.dao.domain.TypeStructure typeStructure = new ru.tsc.task091017.dao.domain.TypeStructure();
        typeStructure.setId(1L);
        typeStructure.setCreated(ts);
        typeStructure.setName("type1");
        typeStructure.setState(1);
        typeStructure.setStructure("structure1");
        typeStructure.setDomain(domain);
        typeStructure.setOwner(user);
        ru.tsc.task091017.dao.domain.FolderTree folder1 = new ru.tsc.task091017.dao.domain.FolderTree(1L, null, "folder1", 1, domain, user, ts);
        ru.tsc.task091017.dao.domain.FolderTree folder11 = new ru.tsc.task091017.dao.domain.FolderTree(2L, folder1, "folder11", 1, domain, user, ts);
        ru.tsc.task091017.dao.domain.FolderTree folder12 = new ru.tsc.task091017.dao.domain.FolderTree(3L, folder1, "folder12", 1, domain, user, ts);
        ru.tsc.task091017.dao.domain.FolderTree folder111 = new ru.tsc.task091017.dao.domain.FolderTree(4L, folder11, "folder111", 1, domain, user, ts);
        ru.tsc.task091017.dao.domain.FolderTree folder112 = new ru.tsc.task091017.dao.domain.FolderTree(5L, folder11, "folder112", 1, domain, user, ts);
        ru.tsc.task091017.dao.domain.Document document = new ru.tsc.task091017.dao.domain.Document();
        document.setId(1L);
        document.setCreated(ts);
        document.setFieldValues("fieldValues1");
        document.setState(1);
        document.setTitle("doc1");
        document.setOwner(user);
        document.setDomain(domain);
        document.setTypeId(typeStructure);
        List<ru.tsc.task091017.dao.domain.FolderTree> folderTs = new ArrayList<>();
        folderTs.add(folder1);
        folderTs.add(folder11);
        folderTs.add(folder12);
        folderTs.add(folder111);
        folderTs.add(folder112);
        document.setFolderId(folderTs);
        ru.tsc.task091017.dao.domain.FolderDocTypeRestriction folderDTR = new ru.tsc.task091017.dao.domain.FolderDocTypeRestriction();
        folderDTR.setId(1L);
        folderDTR.setCreated(ts);
        folderDTR.setDomain(domain);
        folderDTR.setFolderId(folder1);
        folderDTR.setOwner(user);
        folderDTR.setTypeId(typeStructure);

        List<ru.tsc.task091017.dao.domain.Domains> domains = new ArrayList<>();
        domains.add(domain);
        List<ru.tsc.task091017.dao.domain.Users> users = new ArrayList<>();
        users.add(user);
        List<ru.tsc.task091017.dao.domain.TypeStructure> typeStructures = new ArrayList<>();
        typeStructures.add(typeStructure);
        List<ru.tsc.task091017.dao.domain.Document> documents = new ArrayList<>();
        documents.add(document);
        List<ru.tsc.task091017.dao.domain.FolderDocTypeRestriction> folderDTRs = new ArrayList<>();
        folderDTRs.add(folderDTR);

        List<List<?>> allList = new ArrayList<>();
        allList.add(documents);
        allList.add(domains);
        allList.add(folderDTRs);
        allList.add(folderTs);
        allList.add(typeStructures);
        allList.add(users);

        return allList;
    }

    protected static List<ru.tsc.task091017.dao.domain.FolderTreeWrapper> createFolderTreeWrappers() {
        List<ru.tsc.task091017.dao.domain.FolderTreeWrapper> listW = new ArrayList<>();
        List<ru.tsc.task091017.dao.domain.FolderTree> list = (List<ru.tsc.task091017.dao.domain.FolderTree>) createLists().get(3);

        for (ru.tsc.task091017.dao.domain.FolderTree ft : list) {
            listW.add(new ru.tsc.task091017.dao.domain.FolderTreeWrapper(ft));
        }
        listW.get(0).add(listW.get(1));
        listW.get(0).add(listW.get(2));
        listW.get(1).add(listW.get(3));
        listW.get(1).add(listW.get(4));

        return listW;
    }

}
