package ru.tsc.task091017.dao;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.task091017.dao.domain.TypeStructure;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:applicationContextTest.xml")
public class TypeStructureDaoTest extends DaoTest {
    @Autowired
    TypeStructureDao typeStructureDao;

    @SuppressWarnings("unchecked")
    private List<TypeStructure> typeStructures = (List<TypeStructure>) createLists().get(4);

    @Transactional
    @Test
    public void getTypeStructureTest() {
        TypeStructure ts = typeStructureDao.retrieveTypeStructure(1L);
        assertEquals(ts, typeStructures.get(0));
    }

    @Transactional
    @Test
    public void createTypeStructureTest() {
        TypeStructure ts1 = typeStructures.get(0);
        ts1.setId(null);
        ts1.setName("newType");
        typeStructureDao.createTypeStructure(ts1);
        assertEquals(typeStructureDao.retrieveTypeStructure(2L), ts1);
    }

    @Transactional
    @Test
    public void updateTypeStructureTest() {
        TypeStructure ts1 = typeStructures.get(0);
        ts1.setName("newType2");
        typeStructureDao.updateTypeStructure(ts1);
        assertEquals(typeStructureDao.retrieveTypeStructure(1L), ts1);
    }

    @Transactional
    @Test
    public void deleteTypeStructureTest() {
        typeStructureDao.deleteTypeStructure(1L);
        assertNull(typeStructureDao.retrieveTypeStructure(1L));
    }

}
