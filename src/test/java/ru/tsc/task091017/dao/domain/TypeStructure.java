package ru.tsc.task091017.dao.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import org.springframework.context.annotation.Profile;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.sql.Timestamp;
import java.util.List;

/**
 * Created by aalbutov on 09.10.2017.
 */

@Entity
@Table(name = "TypeStructure")
@XmlRootElement(name = "TypeStructure")
@Profile("test")
public class TypeStructure {
    @Id
    @Column(name = "id", nullable = false//, columnDefinition = "NVARCHAR2(36)"
    )
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    @Setter
    private Long id;

    @Column(name = "Name"//, columnDefinition = "NVARCHAR2(100)"
    )
    @Getter
    @Setter
    private String name;

    @Column(name = "Structure"//, columnDefinition = "CLOB"
    )
    @Getter
    @Setter
    private String structure;

    @ManyToOne(targetEntity = ru.tsc.task091017.dao.domain.Domains.class)
    @JoinColumn(name = "domain"//, columnDefinition = "NVARCHAR2(200)"
    )
    @Getter
    @Setter
    private ru.tsc.task091017.dao.domain.Domains domain;

    @ManyToOne(targetEntity = ru.tsc.task091017.dao.domain.Users.class)
    @JoinColumn(name = "owner"//, columnDefinition = "NVARCHAR2(200)"
    )
    @Getter
    @Setter
    private ru.tsc.task091017.dao.domain.Users owner;

    @Column(name = "created", columnDefinition = "TIMESTAMP(6)")
    @Getter
    @Setter
    private Timestamp created;

    @Column(name = "state"//, columnDefinition = "NUMBER(2)"
    )
    @Getter
    @Setter
    private int state;

    @JsonIgnore
    @OneToMany(targetEntity = Document.class, orphanRemoval = false,
            cascade = CascadeType.REMOVE, mappedBy="typeId")
    @Getter @Setter private List<Document> documentList;

    @JsonIgnore
    @OneToMany(targetEntity = ru.tsc.task091017.dao.domain.FolderDocTypeRestriction.class, orphanRemoval = false,
            cascade = CascadeType.MERGE, mappedBy="typeId")
    @Getter @Setter private List<ru.tsc.task091017.dao.domain.FolderDocTypeRestriction> folderDocTypeRestrictionList;

    public TypeStructure(String name, String structure, ru.tsc.task091017.dao.domain.Domains domain, ru.tsc.task091017.dao.domain.Users owner, Timestamp created, int state) {
        this.name = name;
        this.structure = structure;
        this.domain = domain;
        this.owner = owner;
        this.created = created;
        this.state = state;
    }

    public TypeStructure() {

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TypeStructure that = (TypeStructure) o;

        if (state != that.state) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (structure != null ? !structure.equals(that.structure) : that.structure != null) return false;
        if (domain != null ? !domain.equals(that.domain) : that.domain != null) return false;
        if (owner != null ? !owner.equals(that.owner) : that.owner != null) return false;
        return created != null ? created.equals(that.created) : that.created == null;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (structure != null ? structure.hashCode() : 0);
        result = 31 * result + (domain != null ? domain.hashCode() : 0);
        result = 31 * result + (owner != null ? owner.hashCode() : 0);
        result = 31 * result + (created != null ? created.hashCode() : 0);
        result = 31 * result + state;
        return result;
    }

    @Override
    public String toString() {
        return "TypeStructure{" +
                "name='" + name + '\'' +
                ", structure='" + structure + '\'' +
                ", domain=" + domain +
                ", owner=" + owner +
                ", created=" + created +
                ", state=" + state +
                '}';
    }
}
