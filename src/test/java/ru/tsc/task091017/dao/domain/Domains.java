package ru.tsc.task091017.dao.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import org.springframework.context.annotation.Profile;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.sql.Timestamp;
import java.util.List;

/**
 * Created by aalbutov on 09.10.2017.
 */
@Entity
@Table(name="Domains")
@XmlRootElement(name = "Domains")
@Profile("test")
public class Domains {

    @Id
    @Column(name = "id",nullable = false//, columnDefinition = "NVARCHAR2(36)"
    )
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Getter
    @Setter
    private Long id;

    @Column(name = "domain"//, columnDefinition = "NVARCHAR2(200)"
    )
    @Getter @Setter private String domain;

    @Column(name = "created", columnDefinition = "TIMESTAMP(6)")
    @Getter @Setter private Timestamp created;

    @Column(name = "state"//, columnDefinition = "NUMBER(2)"
    )
    @Getter @Setter private int state;

    @JsonIgnore
    @OneToMany(targetEntity = Users.class, orphanRemoval = false,
            cascade = CascadeType.MERGE, mappedBy="domain")
    @Getter @Setter private List<Users> usersList;

    @JsonIgnore
    @OneToMany(targetEntity = ru.tsc.task091017.dao.domain.FolderDocTypeRestriction.class, orphanRemoval = false,
            cascade = CascadeType.MERGE, mappedBy="domain")
    @Getter @Setter private List<ru.tsc.task091017.dao.domain.FolderDocTypeRestriction> folderDocTypeRestrictionList;

    @JsonIgnore
    @OneToMany(targetEntity = ru.tsc.task091017.dao.domain.FolderTree.class, orphanRemoval = false,
            cascade = CascadeType.MERGE, mappedBy="domain")
    @Getter @Setter private List<ru.tsc.task091017.dao.domain.FolderTree> folderTreeList;

    @JsonIgnore
    @OneToMany(targetEntity = ru.tsc.task091017.dao.domain.TypeStructure.class, orphanRemoval = false,
            cascade = CascadeType.MERGE, mappedBy="domain")
    @Getter @Setter private List<ru.tsc.task091017.dao.domain.TypeStructure> typeStructureList;

    @JsonIgnore
    @OneToMany(targetEntity = ru.tsc.task091017.dao.domain.Document.class, orphanRemoval = false,
            cascade = CascadeType.MERGE, mappedBy="domain")
    @Getter @Setter private List<ru.tsc.task091017.dao.domain.Document> documentList;

    public Domains(String domain, Timestamp created, int state) {
        this.domain = domain;
        this.created = created;
        this.state = state;
    }

    public Domains() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Domains domains = (Domains) o;

        if (state != domains.state) return false;
        if (domain != null ? !domain.equals(domains.domain) : domains.domain != null) return false;
        return created != null ? created.equals(domains.created) : domains.created == null;
    }

    @Override
    public int hashCode() {
        int result = domain != null ? domain.hashCode() : 0;
        result = 31 * result + (created != null ? created.hashCode() : 0);
        result = 31 * result + state;
        return result;
    }

    @Override
    public String toString() {
        return "Domains{" +
                "domain='" + domain + '\'' +
                ", created=" + created +
                ", state=" + state +
                '}';
    }
}
