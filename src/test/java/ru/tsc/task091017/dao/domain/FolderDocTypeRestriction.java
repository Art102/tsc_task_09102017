package ru.tsc.task091017.dao.domain;

import lombok.Getter;
import lombok.Setter;
import org.springframework.context.annotation.Profile;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.sql.Timestamp;

/**
 * Created by aalbutov on 09.10.2017.
 */

@Entity
@Table(name="FolderDocTypeRestriction")
@XmlRootElement(name = "FolderDocTypeRestriction")
@Profile("test")
public class FolderDocTypeRestriction {

    @Id
    @Column(name = "id",nullable = false//, columnDefinition = "NVARCHAR2(36)"
    )
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Getter
    @Setter
    private Long id;

    @ManyToOne(targetEntity = Domains.class)
    @JoinColumn(name = "domain"//, columnDefinition = "NVARCHAR2(200)"
    )
    @Getter @Setter private Domains domain;

    @ManyToOne(targetEntity = ru.tsc.task091017.dao.domain.Users.class)
    @JoinColumn(name = "owner"//, columnDefinition = "NVARCHAR2(200)"
    )
    @Getter @Setter private ru.tsc.task091017.dao.domain.Users owner;

    @ManyToOne(targetEntity = ru.tsc.task091017.dao.domain.TypeStructure.class, cascade = CascadeType.MERGE)
    @JoinColumn(name = "typeId"//, columnDefinition = "NVARCHAR2(36)"
    )
    @Getter @Setter private ru.tsc.task091017.dao.domain.TypeStructure typeId;

    @ManyToOne(targetEntity = ru.tsc.task091017.dao.domain.FolderTree.class, cascade = CascadeType.MERGE)
    @JoinColumn(name = "folderId"//, columnDefinition = "NVARCHAR2(36)"
    )
    @Getter @Setter private ru.tsc.task091017.dao.domain.FolderTree folderId;

    @Column(name = "created", columnDefinition = "TIMESTAMP(6)")
    @Getter @Setter private Timestamp created;

    public FolderDocTypeRestriction(Domains domain, ru.tsc.task091017.dao.domain.Users owner, ru.tsc.task091017.dao.domain.TypeStructure typeId, ru.tsc.task091017.dao.domain.FolderTree folderId, Timestamp created) {
        this.domain = domain;
        this.owner = owner;
        this.typeId = typeId;
        this.folderId = folderId;
        this.created = created;
    }

    public FolderDocTypeRestriction() {

    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FolderDocTypeRestriction that = (FolderDocTypeRestriction) o;

        if (domain != null ? !domain.equals(that.domain) : that.domain != null) return false;
        if (owner != null ? !owner.equals(that.owner) : that.owner != null) return false;
        if (typeId != null ? !typeId.equals(that.typeId) : that.typeId != null) return false;
        if (folderId != null ? !folderId.equals(that.folderId) : that.folderId != null) return false;
        return created != null ? created.equals(that.created) : that.created == null;
    }

    @Override
    public int hashCode() {
        int result = domain != null ? domain.hashCode() : 0;
        result = 31 * result + (owner != null ? owner.hashCode() : 0);
        result = 31 * result + (typeId != null ? typeId.hashCode() : 0);
        result = 31 * result + (folderId != null ? folderId.hashCode() : 0);
        result = 31 * result + (created != null ? created.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "FolderDocTypeRestriction{" +
                "domain=" + domain +
                ", owner=" + owner +
                ", typeId=" + typeId +
                ", folderId=" + folderId +
                ", created=" + created +
                '}';
}
}
