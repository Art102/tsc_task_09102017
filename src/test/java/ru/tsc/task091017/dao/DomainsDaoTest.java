package ru.tsc.task091017.dao;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.task091017.dao.domain.Domains;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:applicationContextTest.xml")
public class DomainsDaoTest extends DaoTest {
    @Autowired
    private DomainsDao domainsDao;

    @SuppressWarnings("unchecked")
    private List<Domains> domainsDaos = (List<Domains>) createLists().get(1);

    @Transactional
    @Test
    public void getDomainsTest() {
        Domains domain = domainsDao.retrieveDomains(1L);
        assertEquals(domain, domainsDaos.get(0));
    }

    @Transactional
    @Test
    public void createDomainsTest() {
        Domains domain1 = domainsDaos.get(0);
        domain1.setId(null);
        domain1.setDomain("newDomain");
        domainsDao.createDomains(domain1);
        assertEquals(domainsDao.retrieveDomains(2L), domain1);
    }

    @Transactional
    @Test
    public void updateDomainsTest() {
        Domains domain1 = domainsDaos.get(0);
        domain1.setDomain("newDomain2");
        domainsDao.updateDomains(domain1);
        assertEquals(domainsDao.retrieveDomains(1L), domain1);
    }

    @Transactional
    @Test
    public void deleteDomainsTest() {
        domainsDao.deleteDomains(1L);
        assertNull(domainsDao.retrieveDomains(1L));
    }

}
