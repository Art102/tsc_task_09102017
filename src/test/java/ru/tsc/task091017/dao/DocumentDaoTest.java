package ru.tsc.task091017.dao;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.task091017.dao.domain.TypeStructure;
import ru.tsc.task091017.dao.domain.Document;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:applicationContextTest.xml")
public class DocumentDaoTest extends DaoTest {
    @Autowired
    DocumentDao documentDao;

    @SuppressWarnings("unchecked")
    private List<Document> documents = (List<Document>) createLists().get(0);

    @Transactional
    @Test
    public void getDocumentTest() {
        Document document = documentDao.retrieveDocument(1L);
        assertEquals(document, documents.get(0));
    }

    @Transactional
    @Test
    public void createDocumentTest() {
        Document document1 = documents.get(0);
        document1.setId(null);
        document1.setTitle("newDocument");
        documentDao.createDocument(document1);
        assertEquals(documentDao.retrieveDocument(2L), document1);
    }

    @Transactional
    @Test
    public void updateDocumentTest() {
        Document document1 = documents.get(0);
        document1.setTitle("newDocument2");
        documentDao.updateDocument(document1);
        assertEquals(documentDao.retrieveDocument(1L), document1);
    }

    @Transactional
    @Test
    public void deleteDocumentTest() {
        documentDao.deleteDocument(1L);
        assertNull(documentDao.retrieveDocument(1L));
    }

    @Transactional
    @Test
    public void getDocumentTypesByOwnerTest() {
        List<TypeStructure> ts1 = (List<TypeStructure>) createLists().get(4);
        List<TypeStructure> ts2 = documentDao.getDocumentTypesByOwner(1L);
        if (ts2.size() == 1) {
            assertEquals(ts1.get(0), ts2.get(0));
        } else {
            assertNull("wrong size", ts2);
        }
    }

}
