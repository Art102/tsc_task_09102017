package ru.tsc.task091017.dao;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.task091017.dao.domain.Users;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:applicationContextTest.xml")
public class UsersDaoTest extends DaoTest {
    @Autowired
    private UsersDao usersDao;

    @SuppressWarnings("unchecked")
    private List<Users> users = (List<Users>) createLists().get(5);

    @Transactional
    @Test
    public void getUserTest() {
        Users user = usersDao.retrieveUsers(1L);
        assertEquals(user, users.get(0));
    }

    @Transactional
    @Test
    public void createUserTest() {
        Users user1 = users.get(0);
        user1.setId(null);
        user1.setLogin("newLogin");
        usersDao.createUsers(user1);
        assertEquals(usersDao.retrieveUsers(2L), user1);
    }

    @Transactional
    @Test
    public void updateUserTest() {
        Users user1 = users.get(0);
        user1.setLogin("newLogin2");
        usersDao.updateUsers(user1);
        assertEquals(usersDao.retrieveUsers(1L), user1);
    }

    @Transactional
    @Test
    public void deleteUserTest() {
        usersDao.deleteUsers(1L);
        assertNull(usersDao.retrieveUsers(1L));
    }

}
